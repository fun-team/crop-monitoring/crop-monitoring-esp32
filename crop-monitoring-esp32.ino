// Author : E. PROFIT (etienne.profit@inria.fr)
 
#include "DHT.h"
#include "Adafruit_VEML7700.h"
#include "math.h"
#include "Wire.h"
#include "WiFi.h"
#include "PubSubClient.h"
 
#define D_UART_SPD 115200     //Serial port speed
#define D_I2C_SDA_PIN 18    //I2C SDA Pin
#define D_I2C_SCL_PIN 19    //I2C SCL Pin
#define D_DHTTYPE DHT11     //Type of sensor used for DHT library
#define D_DHTPIN 4          //DHT11 data port on arduino card
#define D_DEW_POINT_A 17.27 //"a" constant used in Magnus' formula for dew point
#define D_DEW_POINT_B 237.7 //"b" constant used in Magnus' formula in Celcius degrees

int SLEEP_CYCLE = 0;

////////// SENSOR PART //////////Archiving built core (caching) in: /tmp/arduino_cache_868642

// Declaring a DHT object to use
DHT dht(D_DHTPIN, D_DHTTYPE);

// Declaring a VEML7700 object to use
Adafruit_VEML7700 veml7700 = Adafruit_VEML7700();

//Heinrich Gustav Magnus' formula's subfunction
float alpha(float T, float phi){
  return (log(phi) + (D_DEW_POINT_A * T)/(D_DEW_POINT_B + T));
}

// Heinrich Gustav Magnus' formula
float dew_point (float T, float phi){
  return ((D_DEW_POINT_B * alpha(T,phi))/(D_DEW_POINT_A - alpha(T,phi)));
}

///////// WiFi PART //////////

const char* ssid     = "esp32-gath";
const char* password = "gathesp!";

String mac = WiFi.macAddress();

WiFiClient wifi;

///////// MQTT PART //////////

PubSubClient mqttClient(wifi); 

char *mqttServer = "192.168.0.1";
int mqttPort = 1883;

////////// INO PART //////////

void setup() {
  Serial.begin(D_UART_SPD);
  Wire.setPins(D_I2C_SDA_PIN,D_I2C_SCL_PIN);
  Wire.begin(); //Joining I2C
     
  // Initialize DHT11 and veml7700 sensors
  dht.begin();
  if (!veml7700.begin()){
    Serial.print("VEML not found");
  }

  // Initialize WiFi connection
  WiFi.begin(ssid);

  //Light Sleep configuration, timer in µs
  esp_sleep_enable_timer_wakeup(3600000000);

  //MQTT configuration
  mqttClient.setServer(mqttServer, mqttPort);
}
 
void loop() {
  float T = 0.0;
  float phi = 0.0;
  float L = 0.0;
  float DEW_POINT = 0.0;

  char data[10];

  T = dht.readTemperature();
  phi = dht.readHumidity();
  L = veml7700.readLux();
  DEW_POINT = dew_point(T,phi/100.0);
  
  while(mqttClient.connected()!=true){
    mqttClient.connect(mac.c_str());
    Serial.print("DEBUG : Connecting to MQTT broker...");  
  }
  Serial.print("DEBUG : Connected to MQTT broker.");
  mqttClient.loop();

  // Sending data

  //Temperature
  sprintf(data,"%f",T);
  const char* temp_topic = (mac+"/temperature").c_str();
  mqttClient.publish(temp_topic, data);

  //Humidity
  sprintf(data,"%f",phi);
  const char* hum_topic = (mac+"/humidity").c_str();
  mqttClient.publish(hum_topic, data);

  //Brightness
  sprintf(data,"%f",L);
  const char* bri_topic = (mac+"/brightness").c_str();
  mqttClient.publish(bri_topic, data);

  //Dew Point
  sprintf(data,"%f",DEW_POINT);
  const char* dew_topic = (mac+"/dewpoint").c_str();
  mqttClient.publish(dew_topic, data);
  
  //mqttClient.disconnect();

  delay(5000);
  Serial.print("DEBUG : Entering light sleep mode...");
  esp_deep_sleep_start();
}
