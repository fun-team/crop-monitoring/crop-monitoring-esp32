# crop-monitoring-station

This project contains some code to use on an ESP32-C3-DevkitC-02 board.

## How to use

To use this file, you have to install a few libraries. The names of these libraries are available in the "requirements.txt" file.

## Features

### Sensors

At this point, two sensors are considered:

-DHT11: temperature and humidity sensor
-VEML7700 : Luminosity sensor

With these two sensors we can gather temperature, brightness and humidity data. The value of the Dew point is calculated with the following formula :

$$T_{dp} = \frac{b*\alpha(T,\phi)}{a - \alpha(T,\phi)}$$

Where :

$T_{dp}$ is the Dew point temperature in Celsius
$T$ is the Measured temperature in Celsius
$\phi$ is the Relative humidity in percent
$a$ and $b$ are two constants :
- $a$=17,27
- $b$=237,7°C

Finally, the $\alpha$ function is:

$$\alpha(T,\phi) = \frac{aT}{b+T}$$

### Connectivity

The flash ESP32 is meant to connect on a wifi network called esp32-gath. This network is deployed by the associate RaspberryPi ; you can find some informations about the network [here](https://gitlab.inria.fr/fun-team/crop-monitoring/crop-monitoring-rpi-gatherer)

### Deep Sleep

The ESP32 will use it's deep sleep mode. It means that it will shut down it's antennas and systems bewteen each data sending. The current sleep time is one hour.
The sleep time can be configured in the program.

### Data gathering

Every hour, the ESP32 wil wake up and gather data from its sensors, then send it over the MQTT broker on the RaspberryPi.

## How to flash ?

To use this program with Arduino IDE, you have to add ESP32 card to your card manager in your preferences adding this line :

https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json

Then, in "Tools/Board", select "ESP32 C3 Dev module".

Then plug your esp32 card and flash it !
